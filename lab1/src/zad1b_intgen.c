#define _XOPEN_SOURCE           500

#define MAX_SIGNALS             4

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

int sig_list[MAX_SIGNALS] = { SIGUSR1, SIGUSR2, SIGILL, SIGSYS };
int sig_count[MAX_SIGNALS] = { 0 };
long pid;

void terminate(int sig) {
    kill(pid, SIGTERM); /*  trazeno je da se posalje SIGKILL,
                            no SIGTERM uzrokuje ispisivanje u
                            programu za obradu prekida        */
    printf(
        "Sent signal count: [%i %i %i %i]\n",
        sig_count[0],
        sig_count[1],
        sig_count[2],
        sig_count[3]
    );
    exit(EXIT_SUCCESS);
}

int main(int argc, char** argv) {

    if (argc != 2) {
        return EXIT_FAILURE;
    }

    if ((pid = atol(argv[1])) == 0) {
        return EXIT_FAILURE;
    }

    sigset(SIGINT, terminate);
    sigset(SIGTERM, terminate);

    srand(time(NULL));

    while (1) {

        int rand_sig, sleep_time = 3 + (rand() % 3);

        while (sleep(sleep_time) > 0) {
            /* cekaj */
        }

        rand_sig = rand() % (sizeof(sig_list) / sizeof(sig_list[0]));
        kill(pid, sig_list[rand_sig]);
        sig_count[rand_sig]++;
        printf("sent signal with priority %i\n", rand_sig + 1);

    }

    return EXIT_SUCCESS;

}
