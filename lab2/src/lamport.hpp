#ifndef __LAMPORT_HPP__
#define __LAMPORT_HPP__

class Lamport {

	public:
		Lamport(int, int = 0);
		virtual ~Lamport();
		void Lock(int);
		void Unlock(int);
		void* getAdditionalMemory();
	
	private:
		int getNewNumber();

		int m_IndexCount;
		int m_SharedMemoryID;
		void* m_SharedMemoryAddr;
		bool* m_RequestingArray;
		int* m_NumberArray;
		int m_AdtMem;
	
};

#endif
