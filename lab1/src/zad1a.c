#define _XOPEN_SOURCE           500

#define TIMER_SECONDS           5
#define TIMER_MICROSECONDS      0

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>

int isPaused = 0;
unsigned long currentNumber = 1000000001;
unsigned long prevPrime = 1000000001;   /*  ovaj broj nije prost,
                                            djeljiv je sa 7     */

void periodic_print(int sig) {
    printf("zadnji prosti broj = %lu\n", prevPrime);
}

void toggle_pause(int sig) {
    isPaused = !isPaused;
}

void terminate(int sig) {
    printf("zadnji ispitivani broj = %lu\n", currentNumber);
    exit(EXIT_SUCCESS); /*  moze i EXIT_FAILURE, zavisi
                            kako gledamo na situaciju   */
}

/* funkcija vraca kriv rezultat za broj 2, ali za >=1000000001 je OK */
int isPrime(unsigned long num) {
    unsigned long i, max;
    if ((num & 1) == 0) {
        return 0;
    }
    max = (unsigned long)sqrt((double)num);
    for (i = 3; i <= max; i += 2) {
        if ((num % i) == 0) {
            return 0;
        }
    }
    return 1;
}

int main() {

    struct itimerval t;

    sigset(SIGALRM, periodic_print);
    sigset(SIGINT, toggle_pause);
    sigset(SIGTERM, terminate);

    t.it_value.tv_sec = TIMER_SECONDS;
	t.it_value.tv_usec = TIMER_MICROSECONDS;
	t.it_interval.tv_sec = TIMER_SECONDS;
	t.it_interval.tv_usec = TIMER_MICROSECONDS;

	setitimer(ITIMER_REAL, &t, NULL);

    while (1) {
        if (isPrime(currentNumber)) {
            prevPrime = currentNumber;
        }
        currentNumber++;
        while (isPaused) {
            pause();
        }
    }

    return EXIT_SUCCESS;

}
