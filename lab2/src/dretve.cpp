#define USE_ATOMIC_VARIABLE			1

#include <iostream>
#include <thread>
#include <vector>
#include <cstdlib>

#if USE_ATOMIC_VARIABLE

	#include <atomic>

	std::atomic_int g_A(0);

#else

	int g_A = 0;

#endif

void thread_routine(int M) {
	for (int i = 0; i < M; i++) {
		g_A++;
	}
}

int main(int argc, char** argv) {

	if (argc != 3) {
		std::cerr << "ERROR: Invalid number of parameters!" << std::endl;
		std::cout << "Usage:   dretve <N> <M>" << std::endl;
		std::cout << " - N = number of threads" << std::endl;
		std::cout << " - M = number of iterations" << std::endl;
		return EXIT_FAILURE;
	}

	int N, M;

	try {
		N = std::stoi(argv[1], nullptr, 10);
		if (N <= 0) {
			throw std::invalid_argument("N must be a positive integer!");
		}
	} catch (const std::exception& e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	try {
		M = std::stoi(argv[2], nullptr, 10);
		if (M <= 0) {
			throw std::invalid_argument("M must be a positive integer!");
		}
	} catch (const std::exception& e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "N = " << N << std::endl << "M = " << M << std::endl;

	std::vector<std::thread> threads;

	for (int i = 0; i < N; i++) {
		try {
			threads.push_back(std::thread(thread_routine, M));
		} catch (const std::exception& e) {
			std::cerr << "ERROR: " << e.what() << std::endl;
			return EXIT_FAILURE;
		}
	}

	for (std::thread& thread : threads) {
		thread.join();
	}

	std::cout << "A = " << g_A << std::endl;

	return EXIT_SUCCESS;

}
