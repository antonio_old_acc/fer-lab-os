#include <stdexcept>

#include "dekker.hpp"

Dekker::Dekker() {
	m_Right = 0;
	m_Flag[0] = m_Flag[1] = false;
}

void Dekker::Lock(int CurrentIndex) {
	
	if ((CurrentIndex < 0) || (CurrentIndex > 1)) {
		throw std::out_of_range("Thread index is out of range.");
	}
	
	int OtherIndex = !CurrentIndex;
	
	m_Flag[CurrentIndex] = true;
	
	while (m_Flag[OtherIndex]) {
		
		if (m_Right == OtherIndex) {
			
			m_Flag[CurrentIndex] = false;
			
			while (m_Right == OtherIndex) {
				// Do nothing...
			}
			
			m_Flag[CurrentIndex] = true;
		}
		
	}
	
}

void Dekker::Unlock(int CurrentIndex) {
	
	if ((CurrentIndex < 0) || (CurrentIndex > 1)) {
		throw std::out_of_range("Thread index is out of range.");
	}
	
	int OtherIndex = !CurrentIndex;
	
	m_Right = OtherIndex;
	m_Flag[CurrentIndex] = false;
	
}
