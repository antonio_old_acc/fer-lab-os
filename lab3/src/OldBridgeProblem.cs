/*
	Ovaj kod je napisan u C#, a za kompilaciju sam koristio Mono.
*/

using System;
using System.Diagnostics;
using System.Threading;

internal enum Direction : int {
	East = 0,
	West = 1
}

internal static class OldBridgeProblem {
	
	public const int MinVehicleArrivalDelay = 2750;
	public const int MaxVehicleArrivalDelay = 3500;
	public const int VehicleDrivingTime = 1000;
	
	public static Random LocalRandom = new Random();
	public static Semaphore LocalSemaphore = new Semaphore(1, 1);
	public static object[] LocalLocks = new object[] {
		new object(), new object()
	};
	public static Direction CurrentDirection = Direction.East;
	public static int[] Pending = new int[] {
		0, 0
	};
	public static int Crossing = 0;
	public static int Crossed = 0;
	
	public static void Main(string[] Args) {
		(new Thread(ConsoleController)).Start();
		int count = 0;
		while (true) {
			int id = ++count;
			Direction direction = (Direction)LocalRandom.Next(0, 2);
			(new Thread(() => Vehicle(id, direction))).Start();
			Thread.Sleep(
				LocalRandom.Next(
					MinVehicleArrivalDelay, MaxVehicleArrivalDelay
				)
			);
		}
	}
	
	public static void ConsoleController() {
		while (true) {
			if (Console.ReadKey().Key == ConsoleKey.Q) {
				Process.GetCurrentProcess().Kill();
			}
		}
	}
	
	public static void Vehicle(int id, Direction direction) {
		
		Direction oppositeDirection = GetOppositeDirection(direction);
		object myLock = LocalLocks[(int)direction];
		object otherLock = LocalLocks[(int)oppositeDirection];
		
		Monitor.Enter(myLock);
		
		Pending[(int)direction]++;
		
		PrintStats();
		Console.Write("Vehicle {0,3:D1} wants to drive ", id);
		Console.ForegroundColor = GetDirectionColor(direction);
		Console.Write(direction.ToString().ToLower());
		Console.ResetColor();
		Console.Write(".     [ ");
		Console.ForegroundColor = ConsoleColor.Yellow;
		Console.Write("WAIT");
		Console.ResetColor();
		Console.WriteLine("  ]");
		
		while ((CurrentDirection != direction) || (Crossing >= 3)) {
			Monitor.Wait(myLock);
		}
		
		Pending[(int)direction]--;
		Crossing++;
		
		PrintStats();
		Console.Write("Vehicle {0,3:D1} started driving ", id);
		Console.ForegroundColor = GetDirectionColor(direction);
		Console.Write(direction.ToString().ToLower());
		Console.ResetColor();
		Console.Write(".    [ ");
		Console.ForegroundColor = ConsoleColor.Green;
		Console.Write("BEGIN");
		Console.ResetColor();
		Console.WriteLine(" ]");
		
		Monitor.Exit(myLock);
		
		Thread.Sleep(VehicleDrivingTime);
		
		Monitor.Enter(myLock);
		
		Crossed++;
		
		PrintStats();
		Console.Write("Vehicle {0,3:D1} drove to the ", id);
		Console.ForegroundColor = GetDirectionColor(direction);
		Console.Write(direction.ToString().ToLower());
		Console.ResetColor();
		Console.Write(".       [ ");
		Console.ForegroundColor = ConsoleColor.Blue;
		Console.Write("END");
		Console.ResetColor();
		Console.WriteLine("   ]");
		
		Monitor.Exit(myLock);
		
		LocalSemaphore.WaitOne();
		if (Crossed >= 3) {
			object targetLock;
			Direction targetDirection;
			if (Pending[(int)direction] > Pending[(int)oppositeDirection]) {
				targetLock = myLock;
				targetDirection = direction;
			} else {
				targetLock = otherLock;
				targetDirection = oppositeDirection;
			}
			Monitor.Enter(targetLock);
			CurrentDirection = targetDirection;
			Crossed = 0;
			Crossing = 0;
			for (int i = 0; i < 3; i++) {
				Monitor.Pulse(targetLock);
			}
			Monitor.Exit(targetLock);
		}
		LocalSemaphore.Release();
		
	}
	
	public static void PrintStats() {
		Console.Write(
			"[" + Direction.East.ToString() + ": {0,3:D1}, " +
			Direction.West.ToString() + ": {1,3:D1}, Crossed: " +
			"{2}, Direction: ",
			Pending[0],
			Pending[1],
			Crossed
		);
		Console.ForegroundColor = GetDirectionColor(CurrentDirection);
		Console.Write(CurrentDirection.ToString());
		Console.ResetColor();
		Console.Write("] ");
	}
	
	public static ConsoleColor GetDirectionColor(Direction direction) {
		switch (direction) {
		case Direction.East:
			return ConsoleColor.Green;
		case Direction.West:
			return ConsoleColor.Red;
		}
		return ConsoleColor.White;
	}
	
	public static Direction GetOppositeDirection(Direction direction) {
		return (direction == Direction.East) ? Direction.West : Direction.East;
	}
	
}
