#define PASS_NUM					5
#define STEP_NUM					5
#define SLEEP_MS					500

#include <iostream>
#include <thread>
#include <vector>
#include <cstdlib>

#include "dekker.hpp"

Dekker g_DekkerMutex;

std::string g_ResetColor("\033[0m");
std::string g_ThreadColors[] = {
	std::string("\033[0;32m"),
	std::string("\033[0;33m")
};

void thread_routine(int Index) {
	
	for (int i = 1; i <= PASS_NUM; i++) {
		
		g_DekkerMutex.Lock(Index);
		
		std::cout << g_ResetColor;
		std::cout << "Thread: " << (Index + 1) << ", Pass Begin." << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_MS));
		
		for (int j = 1; j <= STEP_NUM; j++) {
			
			std::cout << g_ThreadColors[Index];
			std::cout << "Thread: " << (Index + 1) << ", Pass: " << i
				<< ", Step: " << j << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_MS));
			
		}
		
		std::cout << g_ResetColor;
		std::cout << "Thread: " << (Index + 1) << ", Pass End." << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_MS));
		
		g_DekkerMutex.Unlock(Index);
		
	}
	
}

int main(int argc, char** argv) {
	
	std::vector<std::thread> threads;
	
	for (int i = 0; i < 2; i++) {
		threads.push_back(std::thread(thread_routine, i));
	}
	
	for (std::thread& thread : threads) {
		thread.join();
	}
	
	return EXIT_SUCCESS;
	
}
