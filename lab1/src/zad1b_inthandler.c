#define _XOPEN_SOURCE           500

#define MAX_INTS                5
#define MAX_INT_SECONDS         5
#define MAX_PROG_SECONDS        10

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

int int_list[MAX_INTS] = { SIGUSR1, SIGUSR2, SIGILL, SIGSYS, SIGINT };
int int_handling[MAX_INTS] = { 0 };
int int_pending[MAX_INTS] = { 0 };
int int_completed[MAX_INTS] = { 0 };
int current_priority = -1;

void print_stats() {

    /* currently handled interrupt priority */
    printf("   CURR = %i", current_priority + 1);

    /* open interrupt handler threads */
    printf(
        " OPEN_THR = [%c %c %c %c %c]",
        (int_handling[0]) ? '+' : '-',
        (int_handling[1]) ? '+' : '-',
        (int_handling[2]) ? '+' : '-',
        (int_handling[3]) ? '+' : '-',
        (int_handling[4]) ? '+' : '-'
    );

    /* pending interrupt requests remaining */
    printf(
        " PEND_INT = [%i %i %i %i %i]\n",
        int_pending[0],
        int_pending[1],
        int_pending[2],
        int_pending[3],
        int_pending[4]
    );

}

void print_data(int priority, char data) {
    printf(
        " -  %c  %c  %c  %c  %c",
        (priority == 0) ? data : '-',
        (priority == 1) ? data : '-',
        (priority == 2) ? data : '-',
        (priority == 3) ? data : '-',
        (priority == 4) ? data : '-'
    );
    print_stats();
}

void int_disable() {
    int i;
    for (i = 0; i < MAX_INTS; i++) {
        sighold(int_list[i]);
    }
}

void int_enable() {
    int i;
    for (i = 0; i < MAX_INTS; i++) {
        sigrelse(int_list[i]);
    }
}

void int_handle(int priority) {

    int elapsed = 0;

    print_data(priority, 'P');

    while (elapsed < MAX_INT_SECONDS) {
        if (sleep(1) <= 0) {
            print_data(priority, '0' + (char)(++elapsed));
        }
    }

    print_data(priority, 'K');
    int_completed[priority]++;

}

void int_routine(int sig) {

    int i, priority = -1;
    int_disable();

    for (i = 0; i < MAX_INTS; i++) {
        if (sig == int_list[i]) {
            priority = i;
            break;
        }
    }

    if (priority == -1) {
        int_enable();
        return;
    }

    print_data(priority, 'X');
    int_pending[priority]++;

    if (priority > current_priority) {

        current_priority = priority;

        while (current_priority >= 0) {

            int backup_priority = current_priority;

            int_handling[current_priority] = 1;

            while (int_pending[current_priority] > 0) {
                int_enable();
                int_handle(current_priority);
                int_disable();
                current_priority = backup_priority;
                int_pending[current_priority]--;
            }

            int_handling[current_priority] = 0;

            if (int_handling[--current_priority]) {
                break;
            }

        }

    }

    int_enable();
}

void terminate(int sig) {
    printf(
        "Broj obradenih prekida   = [%i %i %i %i %i]\n",
        int_completed[0],
        int_completed[1],
        int_completed[2],
        int_completed[3],
        int_completed[4]
    );
    printf(
        "Broj neobradenih prekida = [%i %i %i %i %i]\n",
        int_pending[0],
        int_pending[1],
        int_pending[2],
        int_pending[3],
        int_pending[4]
    );
    printf(
        "Ukupan broj prekida      = [%i %i %i %i %i]\n",
        int_completed[0] + int_pending[0],
        int_completed[1] + int_pending[1],
        int_completed[2] + int_pending[2],
        int_completed[3] + int_pending[3],
        int_completed[4] + int_pending[4]
    );
    exit(EXIT_FAILURE);
}

int main() {

    int i, elapsed = 0;

    sigset(SIGTERM, terminate);
    for (i = 0; i < MAX_INTS; i++) {
        sigset(int_list[i], int_routine);
    }

    printf("Proces obrade prekida, PID=%ld\n", (long)getpid());
    printf("GP S1 S2 S3 S4 S5\n");
    printf("-----------------\n");

    while (elapsed < MAX_PROG_SECONDS) {
        if (sleep(1) <= 0) {
            printf("%2d  -  -  -  -  -", ++elapsed);
            print_stats();
        }
    }

    printf ("Zavrsio osnovni program\n");

    return EXIT_SUCCESS;

}
