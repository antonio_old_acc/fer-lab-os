all: lab1_all lab2_all lab3_all lab4_all

lab1_all:
	cd lab1 && $(MAKE) all

lab2_all:
	cd lab2 && $(MAKE) all

lab3_all:
	cd lab3 && $(MAKE) all

lab4_all:
	# empty
