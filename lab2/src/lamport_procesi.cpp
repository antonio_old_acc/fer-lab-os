#define SLEEP_MS					500
#define DELAY_MS					200

#define MULTIPLE_TABLES_PER_THREAD	0

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <signal.h>
#include <thread>
#include <unistd.h>
#include <sys/wait.h>

#include "lamport.hpp"

Lamport* g_LamportMutex = nullptr;
int g_TablesNum = 0;
int* g_Tables = nullptr;
int* g_TablesRemaining = nullptr;
bool g_IsChild = false;

#if MULTIPLE_TABLES_PER_THREAD
	int g_TablesFound = 0;
#endif

void print_state() {
	std::cout << "[";
	for (int i = 0; i < g_TablesNum; i++) {
		if (i) {
			std::cout << ", ";
		}
		if (g_Tables[i] >= 0) {
			std::cout << g_Tables[i];
		} else {
			std::cout << "-";
		}
	}
	std::cout << "]" << std::endl;
}

void process_routine(int Index) {
	
	g_IsChild = true;
	
	std::srand(std::time(nullptr) + 256 * Index);
	
	while ((*g_TablesRemaining) > 0) {
		
		std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_MS));
		
		int choice = rand();
		
		for (int i = 0; i < g_TablesNum; i++) {
			
			if (g_Tables[(choice + i) % g_TablesNum] < 0) {
				
				int table = (choice + i) % g_TablesNum;
				std::cout << "\033[0;33m";
				std::cout << "Process " << Index << " picked table " << table
					<< "." << std::endl;
				std::cout << "\033[0m";
				
				std::this_thread::sleep_for(
					std::chrono::milliseconds(SLEEP_MS));
				
				g_LamportMutex->Lock(Index);
				
				if (g_Tables[table] >= 0) {
					std::cout << "\033[0;31m";
					std::cout << "Process " << Index << " failed to "
						<< "reserve table " << table << "." << std::endl;
					std::cout << "\033[0m";
				} else {
					g_Tables[table] = Index;
					(*g_TablesRemaining)--;
					std::cout << "\033[0;32m";
					std::cout << "Process " << Index << " reserved table "
						<< table << ".          ";
					std::cout << "\033[0m";
					print_state();
					
					#if MULTIPLE_TABLES_PER_THREAD
						g_TablesFound++;
					#else
						g_LamportMutex->Unlock(Index);
						return;
					#endif
					
				}
				
				g_LamportMutex->Unlock(Index);
				
			}
			
		}
		
	}
	
	#if MULTIPLE_TABLES_PER_THREAD
		std::cout << "\033[0;34m";
		std::cout << "Process " << Index << " finished with " << g_TablesFound
			<< " tables reserved." << std::endl;
	#else
		std::cout << "\033[0;31m";
		std::cout << "Process " << Index << " could not find a table." << std::endl;
	#endif
	
	std::cout << "\033[0m";
	
}

void terminate(int sig) {
	if (!g_IsChild) {
		delete g_LamportMutex;
		std::cout << "Terminated. Shared memory freed." << std::endl;
	}
	exit(EXIT_FAILURE);
}

int main(int argc, char** argv) {
	
	if (argc != 3) {
		std::cerr << "ERROR: Invalid number of parameters!" << std::endl;
		std::cout << "Usage:   lamport_procesi <nProcs> <nTables>" << std::endl;
		std::cout << " - nProcs = number of processes" << std::endl;
		std::cout << " - nTables = number of tables" << std::endl;
		return EXIT_FAILURE;
	}
	
	int nProcs;
	
	try {
		nProcs = std::stoi(argv[1], nullptr, 10);
		if (nProcs <= 0) {
			throw std::invalid_argument("nProcs must be a positive integer!");
		}
	} catch (const std::exception& e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	
	try {
		g_TablesNum = std::stoi(argv[2], nullptr, 10);
		if (g_TablesNum <= 0) {
			throw std::invalid_argument("nTables must be a positive integer!");
		}
	} catch (const std::exception& e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	
	try {
		g_LamportMutex = new Lamport(nProcs, (g_TablesNum + 1) * sizeof(int));
	} catch (const std::exception& e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	
	sigset(SIGINT, terminate);
	sigset(SIGTERM, terminate);
	sigset(SIGQUIT, terminate);
	
	g_TablesRemaining =
		reinterpret_cast<int*>(g_LamportMutex->getAdditionalMemory());
	(*g_TablesRemaining) = g_TablesNum;
	g_Tables = &(g_TablesRemaining[1]);
	std::memset(g_Tables, -1, g_TablesNum * sizeof(int));

	for (int i = 0; i < nProcs; i++) {
		
		switch (fork()) {
		case 0:
			process_routine(i);
			return EXIT_SUCCESS;
		case -1:
			std::cerr << "ERROR: Process could not be forked." << std::endl;
			delete g_LamportMutex;
			return EXIT_FAILURE;
		}
		
		std::this_thread::sleep_for(std::chrono::milliseconds(DELAY_MS));
		
	}
	
	for (int i = 0; i < nProcs; i++) {
		wait(nullptr);
	}
	
	delete g_LamportMutex;
	
	return EXIT_SUCCESS;
	
}
