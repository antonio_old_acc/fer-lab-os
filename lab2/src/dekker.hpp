#ifndef __DEKKER_HPP__
#define __DEKKER_HPP__

#include <atomic>

class Dekker {

	public:
		Dekker();
		void Lock(int);
		void Unlock(int);
	
	private:
		std::atomic_int m_Right;
		std::atomic_bool m_Flag[2];
	
};

#endif
