/*
	Ovaj kod je napisan u C#, a za kompilaciju sam koristio Mono.
*/

using System;
using System.Diagnostics;
using System.Threading;

internal enum Ingredient : int {
	None = 0,
	Paper = 1,
	Tobacco = 2,
	Matches = 3
}

internal static class CigaretteProblem {
	
	public const int ThreadCreationDelay = 1000;
	public const int SmokingDuration = 5000;
	public const int TakingIngredientsDuration = 500;
	public const int BringingIngredientsDuration = 1500;
	
	public static Random LocalRandom = new Random();
	public static Ingredient[] Table = new Ingredient[] {
		Ingredient.None, Ingredient.None
	};
	public static Semaphore DoneTaking = new Semaphore(0, 1);
	public static Semaphore[] Semaphores = new Semaphore[] {
		new Semaphore(0, 1),
		new Semaphore(0, 1),
		new Semaphore(0, 1)
	};
	public static Thread[] SmokerThreads = new Thread[] {
		new Thread(() => Smoker(Ingredient.Paper)),
		new Thread(() => Smoker(Ingredient.Tobacco)),
		new Thread(() => Smoker(Ingredient.Matches))
	};
	
	public static void Main(string[] Args) {
		(new Thread(ConsoleController)).Start();
		foreach (Thread smokerThread in SmokerThreads) {
			Thread.Sleep(ThreadCreationDelay);
			smokerThread.Start();
		}
		Thread.Sleep(ThreadCreationDelay);
		Salesman();
	}
	
	public static void ConsoleController() {
		while (true) {
			if (Console.ReadKey().Key == ConsoleKey.Q) {
				Process.GetCurrentProcess().Kill();
			}
		}
	}
	
	public static void Salesman() {
		while (true) {
			Thread.Sleep(BringingIngredientsDuration);
			
			GenerateRandomIngredients(out Table[0], out Table[1]);
			Ingredient remaining = GetRemainingIngredient(Table[0], Table[1]);
			
			Console.ForegroundColor = ConsoleColor.Blue;
			Console.Write("\nSalesman");
			Console.ResetColor();
			Console.Write(" has just put ");
			Console.ForegroundColor = GetIngredientColor(remaining);
			Console.Write(Table[0].ToString().ToLower());
			Console.ResetColor();
			Console.Write(" & ");
			Console.ForegroundColor = GetIngredientColor(remaining);
			Console.Write(Table[1].ToString().ToLower());
			Console.ResetColor();
			Console.WriteLine(" on the table.");
			
			Semaphores[(int)remaining - 1].Release();// Postavi BSEM[Semafor_i]
			DoneTaking.WaitOne();	// Cekaj BSEM[DoneTaking]
		}
	}
	
	public static void Smoker(Ingredient ingredient) {
		Semaphore mySemaphore = Semaphores[(int)ingredient - 1];
		
		Console.Write("  Smoker ");
		Console.ForegroundColor = GetIngredientColor(ingredient);
		Console.Write(((int)ingredient).ToString());
		Console.ResetColor();
		Console.Write(" has ");
		Console.ForegroundColor = GetIngredientColor(ingredient);
		Console.Write(ingredient.ToString().ToLower());
		Console.ResetColor();
		Console.WriteLine(".");
		
		while (true) {
			mySemaphore.WaitOne();	// Cekaj BSEM[Semafor_i]
			
			Thread.Sleep(TakingIngredientsDuration);
			
			if (GetCigarettesCount(ingredient, Table[0], Table[1]) != 1) {
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(
					"ERROR: Invalid ingredients for smoker " + 
					((int)ingredient).ToString() + "!"
				); // Ovo se nece NIKADA dogoditi :)
			}
			
			Table[0] = Ingredient.None;
			Table[1] = Ingredient.None;
			
			Console.Write("  Smoker ");
			Console.ForegroundColor = GetIngredientColor(ingredient);
			Console.Write(((int)ingredient).ToString());
			Console.ResetColor();
			Console.Write(" has taken the ");
			Console.ForegroundColor = GetIngredientColor(ingredient);
			Console.Write("ingredients");
			Console.ResetColor();
			Console.WriteLine(" from the table and is now smoking...");
			
			DoneTaking.Release();	// Postavi BSEM[DoneTaking]
			
			Thread.Sleep(SmokingDuration);
		}
	}
	
	public static ConsoleColor GetIngredientColor(Ingredient ingredient) {
		switch (ingredient) {
		case Ingredient.Paper:
			return ConsoleColor.Yellow;
		case Ingredient.Tobacco:
			return ConsoleColor.Green;
		case Ingredient.Matches:
			return ConsoleColor.Red;
		}
		return ConsoleColor.White;
	}
	
	public static void GenerateRandomIngredients(
		out Ingredient firstIngredient,
		out Ingredient secondIngredient
	) {
		firstIngredient = (Ingredient)LocalRandom.Next(1, 4);
		secondIngredient = (Ingredient)LocalRandom.Next(1, 4);
		if (secondIngredient == firstIngredient) {
			secondIngredient = (Ingredient)(((int)firstIngredient % 3) + 1);
		}
	}
	
	public static Ingredient GetRemainingIngredient(
		Ingredient firstIngredient,
		Ingredient secondIngredient
	) {
		if (((firstIngredient == Ingredient.Paper) &&
			(secondIngredient == Ingredient.Tobacco)) ||
			((firstIngredient == Ingredient.Tobacco) &&
			(secondIngredient == Ingredient.Paper))) {
			return Ingredient.Matches;
		} else if (((firstIngredient == Ingredient.Paper) &&
			(secondIngredient == Ingredient.Matches)) ||
			((firstIngredient == Ingredient.Matches) &&
			(secondIngredient == Ingredient.Paper))) {
			return Ingredient.Tobacco;
		} else if (((firstIngredient == Ingredient.Tobacco) &&
			(secondIngredient == Ingredient.Matches)) ||
			((firstIngredient == Ingredient.Matches) &&
			(secondIngredient == Ingredient.Tobacco))) {
			return Ingredient.Paper;
		}
		return Ingredient.None;
	}
	
	public static int GetCigarettesCount(params Ingredient[] ingredients) {
		int[] count = new int[3];
		foreach (Ingredient ingredient in ingredients) {
			if (ingredient != Ingredient.None) {
				count[(int)ingredient - 1]++;
			}
		}
		return Math.Min(Math.Min(count[0], count[1]), count[2]);
	}
	
}
