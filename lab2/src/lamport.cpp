#include <stdexcept>
#include <cstring>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "lamport.hpp"

Lamport::Lamport(int IndexCount, int AdditionalMemory) {
	
	m_AdtMem = AdditionalMemory;
	m_IndexCount = IndexCount;
	m_SharedMemoryID = shmget(IPC_PRIVATE,
		m_IndexCount * (sizeof(bool) + sizeof(int)) + m_AdtMem, 0600);
	
	if (m_SharedMemoryID == -1) {
		throw std::runtime_error("Shared memory could not be acquired.");
	}
	
	m_SharedMemoryAddr = shmat(m_SharedMemoryID, nullptr, 0);
	std::memset(m_SharedMemoryAddr, 0,
		m_IndexCount * (sizeof(bool) + sizeof(int)) + m_AdtMem);
	
	m_RequestingArray = reinterpret_cast<bool*>(m_SharedMemoryAddr);
	m_NumberArray = reinterpret_cast<int*>(&(m_RequestingArray[m_IndexCount]));
}

Lamport::~Lamport() {
	shmdt(m_SharedMemoryAddr);
	shmctl(m_SharedMemoryID, IPC_RMID, nullptr);
}

void Lamport::Lock(int CurrentIndex) {
	
	if ((CurrentIndex < 0) || (CurrentIndex >= m_IndexCount)) {
		throw std::out_of_range("Thread index is out of range.");
	}
	
	m_RequestingArray[CurrentIndex] = true;
	m_NumberArray[CurrentIndex] = getNewNumber();
	m_RequestingArray[CurrentIndex] = false;
	
	for (int i = 0; i < m_IndexCount; i++) {
		
		while (m_RequestingArray[i]) {
			// Do nothing ...
		}
		
		while (m_NumberArray[i] &&
			((m_NumberArray[i] < m_NumberArray[CurrentIndex]) || 
			((m_NumberArray[i] == m_NumberArray[CurrentIndex]) &&
			(i < CurrentIndex)))) {
			// Do nothing ...
		}
		
	}
	
}

void Lamport::Unlock(int CurrentIndex) {
	
	if ((CurrentIndex < 0) || (CurrentIndex >= m_IndexCount)) {
		throw std::out_of_range("Thread index is out of range.");
	}
	
	m_NumberArray[CurrentIndex] = 0;
	
}

void* Lamport::getAdditionalMemory() {
	return reinterpret_cast<void*>(&(m_NumberArray[m_IndexCount]));
}

int Lamport::getNewNumber() {
	
	int number = 0;
	
	for (int i = 0; i < m_IndexCount; i++) {
		if (m_NumberArray[i] > number) {
			number = m_NumberArray[i];
		}
	}
	
	return number + 1;
	
}
